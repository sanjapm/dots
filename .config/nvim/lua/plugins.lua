-- Packer

return require('packer').startup(function()

    use "wbthomason/packer.nvim"
    use "ggandor/lightspeed.nvim"

end)
